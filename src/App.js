import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Calculator } from './features/calculator/Calculator';
import './App.css';

function App() {
  return (
    <>
      <Calculator />
    </>
  );
}

export default App;
