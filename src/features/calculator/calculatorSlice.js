import { createSlice } from '@reduxjs/toolkit';
import {DIVISION, ASSIGNMENT, MULTIPLICATION, AC, CE, MPLUS, MMINUS} from './constants';
export const counterSlice = createSlice({
  name: 'calculator',
  initialState: {
    display: ""
  },
  reducers: {
    operator: (state, action) => {
      switch (action.payload) {
        case AC:
          state.display = '';
          break;

        case ASSIGNMENT:
          let display = state.display;
          let reDivision = new RegExp(DIVISION,"g");
          let reMultipcation = new RegExp(MULTIPLICATION,"g");
          try {
            display = display.replace(reDivision, '/');
            display = display.replace(reMultipcation, '*');;
            state.display = eval(display);
          } catch (e) {
            state.display = 'ERROR!!!!';
            console.log(e);
          }
          break;

        default:
          state.display = state.display + '' + action.payload;
      }

    },
  },
});

export const { operator } = counterSlice.actions;

export const display = state => state.calculator.display;

export default counterSlice.reducer;
