import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { DIVISION, MULTIPLICATION, ADDITION, ASSIGNMENT, SUBTRACTION, DOT, AC } from './constants';
import {
  operator,
  display
} from './calculatorSlice';

import styles from './Calculator.module.css';

import {Button, Col, FormControl, Row, Container} from "react-bootstrap";

const keys = [
  [AC],
  [7,8,9,ADDITION],
  [4,5,6,SUBTRACTION],
  [1,2,3,MULTIPLICATION],
  [0,DOT,ASSIGNMENT,DIVISION]
];

export function Calculator() {
  let displayResult = useSelector(display);
  const dispatch = useDispatch();
  return (
    <Container fluid="md">
      <div className={styles['calculator-container']}>
        <Row className="justify-content-md-center">
          <Col>
            <FormControl className={styles['display']} value={displayResult}/>
            <br />
          </Col>
        </Row>
        {keys.map((row) => (
          <Row className="justify-content-md-center">
            {row.map((key)=>(
              <Col key={key} className={styles['key-column']} >
                <Button onClick={() => dispatch(operator(key))} size="lg">{key}</Button>
              </Col>
            ))}
          </Row>
        ))}
      </div>
    </Container>
  );
}
